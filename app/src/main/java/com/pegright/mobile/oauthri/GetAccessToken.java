package com.pegright.mobile.oauthri;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import android.os.AsyncTask;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;
import android.util.Log;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;

/**
 * 
 * @author PEGRight 2015
 * 
 * This is a custom AsyncTask that is used to exchange the authentication
 * code for an access token.  Following successful exchange the resulting
 * JSON Object that contains all of the information is returned to the
 * Authentication Complete listener that was configured at initialization.
 *
 */
public class GetAccessToken extends AsyncTask<String, Void, JSONObject> {

	String tokenUrl;
	String authCode;
	AuthenticationComplete listener;
	
	/**
	 * The constructor for the AsyncTask that stores that important variables
	 * for later use following when the AsyncTask is started.
	 * 
	 * @param tokenUrl  The URL of the end point for the code to token exchange.
	 * @param authCode  The authorization code to exchange for an access token.
	 * @param listener  A listener to call back into following the request.
	 */
	public GetAccessToken(String tokenUrl, String authCode, AuthenticationComplete listener) {
		this.tokenUrl = tokenUrl;
		this.authCode = authCode;
		this.listener = listener;
	}
	
	/**
	 * This is the method called on AsyncTask execute.  This will make the request
	 * to the configured URL to exchange the authorization code for the access token
	 * payload.
	 * 
	 * @param params  The parameters of the call (empty in this case).
	 * 
	 * @returns JSONObject  The JSON payload contains the access token and related information.
	 */
	@Override
	protected JSONObject doInBackground(String... params) {
		JSONObject failRet = new JSONObject();
		String failReason = "";
		HttpClient httpClient = new DefaultHttpClient();
		HostnameVerifier hostnameVerifier = org.apache.http.conn.ssl.SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER;
		HttpsURLConnection.setDefaultHostnameVerifier(hostnameVerifier);
		HttpPost httpPost = new HttpPost(this.tokenUrl);
		String grant_type = "authorization_code";
        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
        nameValuePairs.add(new BasicNameValuePair("grant_type", grant_type));
        nameValuePairs.add(new BasicNameValuePair("code", this.authCode));
        try {
        	String responseBody = "";
            httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            HttpResponse response = httpClient.execute(httpPost);
            //harvest out the JSON Response
            responseBody = EntityUtils.toString(response.getEntity());
            return new JSONObject(responseBody);
        } catch(ClientProtocolException e) {
        	failReason = "Failed Client EX" + e.toString();
        } catch(IOException e) {
			//String stackTrace = Log.getStackTraceString(e);
			failReason = "Failed IO EX" + e.toString();
        } catch(Exception e) {
        	failReason = "Failed EX : " + e.toString();
        }
        try {
			failRet.put("access_token", failReason);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return failRet;
	}
	
	/**
	 * This is the method called when the doInBackground method finishes.  Here
	 * we use the JSON Object returned from the POST in doInBackground to return
	 * the JSON Object to the listener.
	 * 
	 * @param result  The JSON Object returned by the POST in doInBackground
	 */
	@Override
    protected void onPostExecute(JSONObject result) {
        listener.onAuthComplete(result);
    }
	

}
