package com.pegright.mobile.oauthri;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.AsyncTask;

/**
 * 
 * @author PEGRight 2015
 *
 * This is a cutsom AsyncTask that will validate an access token and 
 * return the resulting JSON Object with information about the user
 * and the client application upon successful validation.
 */
public class ValidateToken extends AsyncTask<String, Void, JSONObject> {
	
	String accessToken;
	String validateUrl;
	ValidationComplete listener;
	
	/**
	 * The constructor for the AsyncTask that stores that important variables
	 * for later use following when the AsyncTask is started.
	 * 
	 * @param accessToken  The access token to validate
	 * @param validateUrl  The URL of the end point used for validation
	 * @param listener  The listener to call back into following completion of the task
	 */
	public ValidateToken(String accessToken, String validateUrl, ValidationComplete listener) {
		this.accessToken = accessToken;
		this.validateUrl = validateUrl;
		this.listener = listener;
	}

	/**
	 * This is the method called on AsyncTask execute.  This will make the request
	 * to the configured URL to validate the access token and return information
	 * about the user and the client application.
	 * 
	 * @param params  The parameters of the call (empty in this case).
	 * 
	 * @returns JSONObject  The JSON payload contains information about the user and
	 * client application represented by the access token.
	 */
	@Override
	protected JSONObject doInBackground(String... params) {
		JSONObject failRet = new JSONObject();
		String failReason = "";
		HttpClient httpClient = new DefaultHttpClient();
		HttpPost httpPost = new HttpPost(this.validateUrl);
        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
        nameValuePairs.add(new BasicNameValuePair("token", this.accessToken));
        try {
        	String responseBody = "";
            httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            HttpResponse response = httpClient.execute(httpPost);
            //harvest out the JSON Response
            responseBody = EntityUtils.toString(response.getEntity());
            return new JSONObject(responseBody);
        } catch(ClientProtocolException e) {
        	failReason = "Failed Client EX";
        } catch(IOException e) {
        	failReason = "Failed IO EX";
        } catch(Exception e) {
        	failReason = "Failed EX : " + e.toString();
        }
        try {
			failRet.put("firstName", failReason);
		} catch (JSONException e) {

			e.printStackTrace();
		}
        return failRet;
	}
	
	/**
	 * This is the method called when the doInBackground method finishes.  Here
	 * we use the JSON Object returned from the POST in doInBackground to return
	 * the JSON Object to the listener.
	 * 
	 * @param result  The JSON Object returned by the POST in doInBackground
	 */
	@Override
    protected void onPostExecute(JSONObject result) {
        listener.onValidationComplete(result);
    }

}
