package com.pegright.mobile.oauthri;

import org.json.JSONObject;

/**
 * @author PEGRight 2015
 *
 * This is the interface that will be used by the Authentication 
 * ASync Task to call back into the correct activity.
 */
public interface AuthenticationComplete {

	void onAuthComplete(JSONObject response);
}
