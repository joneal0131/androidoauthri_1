package com.pegright.mobile.oauthri;

import org.json.JSONObject;

/**
 * @author PEGRight 2015
 *
 * This is the interface that will be used by the Validation 
 * ASync Task to call back into the correct activity.
 */
public interface ValidationComplete {

	void onValidationComplete(JSONObject response);
	
}
