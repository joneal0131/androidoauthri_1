package com.pegright.mobile.oauthri;

import android.app.PendingIntent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.app.Activity;
import android.app.ProgressDialog;
import android.support.customtabs.CustomTabsIntent;
import android.support.customtabs.*;
import android.support.design.widget.FloatingActionButton;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.content.Intent;
import android.net.Uri;
import android.widget.Button;
import android.widget.TextView;
import android.view.View.OnTouchListener;
import android.widget.Toast;






import org.json.JSONObject;
import org.json.JSONException;


import java.util.Iterator;
//import android.app.ActionBar;
import android.support.customtabs.CustomTabsIntent;
import android.widget.Toast.*;


import net.openid.appauth.*;

import kotlin.data;


/**
 * @author PEGRight 2015
 *
 * This is the main activity that starts when the application begins.  
 */
public class MainActivity extends Activity implements AuthenticationComplete, ValidationComplete {

    private String accessToken = "Failed";
    private String firstName = "";

    ProgressDialog pdialog;

    /**
     * This is the entry point into the application called when the activity is created.  In this
     * case we will use the entry to start another activity that will open a WebView for authentication.
     *
     * @param savedInstanceState Can be used for re-initialization in the case where state was saved.
     * @return void
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //pdialog = new ProgressDialog(this);
        //pdialog.setCancelable(true);
        //pdialog.setMessage("Loading");
        //pdialog.show();onAc
        setContentView(R.layout.activity_main);


        //if(chromeInstalled()){
        //CustomTabsIntent.Builder x = new CustomTabsIntent.Builder();
        //x.build().intent.setData(Uri.parse(getString(R.string.start_oauth_url) + "?client_id=" + getString(R.string.oauth_client_id)));

        //startActivityForResult(x, 217);

        //CustomTabsIntent.Builder intent1 = new CustomTabsIntent.Builder();
        // x.setToolbarColor(Color.parseColor("#3F51B5"));
        // x.setShowTitle(true);
        //CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
        //CustomTabsIntent customTabsIntent = builder.build();

        //openUrlForResult(customTabsIntent, 215);


        // x.build().intent.setPackage("com.android.chrome");
        // x.build().launchUrl(this, Uri.parse(getString(R.string.start_oauth_url) + "?client_id=" + getString(R.string.oauth_client_id)));
        //}else{
        Intent intent = new Intent(this, WebViewActivity.class);
        intent.setData(Uri.parse(getString(R.string.start_oauth_url) + "?client_id=" + getString(R.string.oauth_client_id)));
        startActivityForResult(intent, 216);
//intent.setData(Uri.parse(getString(R.string.start_oauth_url) + "?client_id=" + getString(R.string.oauth_client_id)));
        // }



        // }
        //decide to use chrometabs or go to webview at startActivityForResult(intent,216)


        //startActivityForResult(intent, 216);

        //public void ButtonClicker(View v) {
        //   Toast.makeText(this, "Button Clicked", Toast.LENGTH_LONG).show();
        // openCustomTab();

        }


    private boolean chromeInstalled() {
        try {
            getPackageManager().getPackageInfo("com.android.chrome", 0);
            return true;


        } catch (Exception e) {
            return false;

        }
    }


    /**
     * This is the method that will get called on the completion of authentication.  This
     * will serve as the entry point into the application so all code that was previously
     * in the onCreate should be moved to here after the access token has been procured.
     *
     * @param response The JSON object that is the result of the access token retrieval.
     * @return void
     */
    public void onAuthComplete(JSONObject response) {
        try {
            accessToken = response.getString("access_token");
        } catch (JSONException e) {
            accessToken = "Failed JSON Exception";
        }
        //TextView textView = (TextView)findViewById(R.id.TextMsg);
        //textView.setText(getString(R.string.auth_msg) + "     Your Access Token is:  " + accessToken);
        //pdialog.dismiss();
        validateToken(findViewById(android.R.id.content));
    }

    /**
     * This is the method that will be called on the completion of the validation.  If
     * the token is valid then the JSON Object returned will contain information about
     * the user for which the token was requested and the client that requested the token.
     *
     * @param response The JSON object containing the information about the user and the
     *                 client application that the token represents.
     * @return void.
     */
    //??Display table where validation complete displays on screen
    public void onValidationComplete(JSONObject response) {
        JSONObject accessTknJSON = new JSONObject();
        String attributes = "";
        try {
            accessTknJSON = response.getJSONObject("access_token");
            firstName = accessTknJSON.getString("firstName");
        } catch (JSONException e) {
            firstName = e.toString();
        }
        TextView textView = (TextView) findViewById(R.id.welcomeMsg);
        textView.setText("Welcome  " + firstName + "!" + attributes);

        textView = (TextView) findViewById(R.id.attrName0);
        textView.setText("Access Token:");
        textView = (TextView) findViewById(R.id.attrValue0);
        textView.setText(accessToken);
        textView = (TextView) findViewById(R.id.attrName1);
        textView.setText("JSON:");
        textView = (TextView) findViewById(R.id.attrValue1);
        textView.setText(accessTknJSON.toString());

        if (accessTknJSON != null) {
            Iterator<?> keys = accessTknJSON.keys();
            Object o = new Object();
            int row = 2;
            while (keys.hasNext()) {
                String key = (String) keys.next();
                try {
                    o = accessTknJSON.get(key);
                } catch (JSONException e) {
                    attributes = e.toString();
                }
                //attributes += "\n" + key + " " + o.toString();
                String nameField = "attrName" + row;
                String valueField = "attrValue" + row;
                textView = (TextView) findViewById(getResources().getIdentifier(nameField, "id", getPackageName()));
                textView.setText(key + ":");
                textView = (TextView) findViewById(getResources().getIdentifier(valueField, "id", getPackageName()));
                textView.setText(o.toString());
                row++;
            }
        }

//        pdialog.dismiss();
    }

    /**
     * The callback for the activities called by this activity that require a result.
     * For example the activity that starts the WebView to retrieve the authentication
     * code for use by the application.
     *
     * @param requestCode The request code used to verify the activity that returned
     *                    the result.
     * @param resultCode  A value containing a status of the returned activity.
     * @param data        The intent data used to share information between acitivites.
     */

    //??Request AccessToken from GetAccessToken then coverts
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //Make sure the result is from our web activity and that the status is good
        if ((requestCode == 216) && resultCode == RESULT_OK) {
            if (data != null) {
                //grab the access token and make the post call to the conversion servlet
                Log.d("com.pegright.mobile.o", "Auth Code: " + data.getStringExtra(getString(R.string.auth_code_extra)));

                GetAccessToken convert = new GetAccessToken(getString(R.string.auth_code_exchange_url), data.getStringExtra(getString(R.string.auth_code_extra)), this);
                convert.execute();
            }
        }

    }

    /**
     * The event listener to validate the access token.
     *
     * @param view The view that was clicked
     */
    public void validateToken(View view) {
        ValidateToken validate = new ValidateToken(accessToken, getString(R.string.token_validate_url), this);
        validate.execute();
    }

    /**
     * Creates the menu options from the configuration file.
     *
     * @param menu The menu to inflate
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }


    /**
     * Handles clicks on the menu.
     *
     * @param item The MenuItem that was selected.
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void openUrlForResult(CustomTabsIntent customTabsIntent, int requestCode) {
        customTabsIntent.intent.setData(Uri.parse(getString(R.string.start_oauth_url) + "?client_id=" + getString(R.string.oauth_client_id)));

        startActivityForResult(customTabsIntent.intent, requestCode);
    }

}
