package com.pegright.mobile.oauthri;

import android.app.Activity;
import android.app.ProgressDialog;
import android.net.Uri;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.ConsoleMessage;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.webkit.CookieSyncManager;
import android.webkit.CookieManager;
import android.content.Intent;
import android.graphics.Bitmap;
import android.util.Log;
import android.text.format.Time;
import android.support.customtabs.CustomTabsIntent;
import android.support.customtabs.*;
import android.widget.Toast;
import java.net.*;
import java.util.*;


import com.novoda.simplechromecustomtabs.SimpleChromeCustomTabs;
import com.novoda.simplechromecustomtabs.navigation.NavigationFallback;

import kotlin.data;


/**
 * @author PEGRight 2015
 *         <p/>
 *         This is the Activity that creates a CustomTabs to retrieve the authentication code
 *         for the application via cookie transport.
 */
public class WebViewActivity extends Activity {

    ProgressDialog pdialog;

    /**
     * The entry point of the activity when called.  This is where we create
     * the WebView and the WebViewClient that will monitor for the authentication
     * code cookie to be created to return the code to the calling Activity.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        pdialog = new ProgressDialog(this);
        pdialog.setCancelable(true);
        pdialog.setMessage("Loading");
        pdialog.show();
        SimpleChromeCustomTabs.initialize(this);
        //Create the webview and start the OAuth flow
        WebView webview = new WebView(this);
        setContentView(webview);

        Intent intent = getIntent();
        Log.d("com.pegright.mobile.o", "WebView");
        //enable javascript

        webview.getSettings().setJavaScriptEnabled(true);
        webview.getSettings().setSupportMultipleWindows(false);
        webview.getSettings().setSupportZoom(false);
        webview.setVerticalScrollBarEnabled(false);
        webview.setHorizontalScrollBarEnabled(false);
        webview.getSettings().setUserAgentString("Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; rv:11.0) like Gecko");
        webview.setWebChromeClient(new WebChromeClient() {

            @Override
            public boolean onConsoleMessage(ConsoleMessage conMesg) {
                Log.e("com.pegright.mobile.o", conMesg.message());
                return true;
            }
        });


        webview.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return (event.getAction() == MotionEvent.ACTION_MOVE);
            }
        });


        //create the listener to determine if the cookie has been created that
        //represents the authentication code
        webview.setWebViewClient(new WebViewClient() {

            @Override
            public void onPageFinished(WebView view, String url ) {
                Time now = new Time();
                now.setToNow();
                Log.d("com.pegright.mobile.o", "Finished Page Load for: " + url + " at " + now.toString());
                //Log.d("com.pegright.mobile.o", "Return authcode" + authCode);

                pdialog.dismiss();
                //Get the cookie manager and check for our access token cookie
                CookieSyncManager.getInstance().sync();
                String cookie = CookieManager.getInstance().getCookie(url);
                Log.d("com.pegright.mobile.o", " url after cookie instance: " + url);


                if (cookie != null) {
                   String[] cookies = cookie.split(";");
                    for (int i = 0; i < cookies.length; i++) {
                        String[] parts = cookies[i].split("=", 2);
                        //see if we have found our cookie
                       if (parts.length == 2 && parts[0].trim().equalsIgnoreCase(getString(R.string.access_token_cookie))) {
                           Log.d("com.pegright.mobile.o", " access_token_cookie: " + getString(R.string.access_token_cookie));
                           Intent result = new Intent();
                            //clean the cookies to prevent persistence issues
                            CookieManager.getInstance().removeAllCookie();
                            //build response

                            result.putExtra(getString(R.string.auth_code_extra), parts[1]);
                           Log.d("com.pegright.mobile.o", "Auth Code in OnPageFinished: " +  result.getStringExtra(getString(R.string.auth_code_extra)));

                            setResult(RESULT_OK, result);


                            //calling finish will destroy this activity and call the onActivityResult of the calling activity
                            Log.d("com.pegright.mobile.o", "Found access token cookie: " + getString(R.string.access_token_cookie));
                            finish();
                            return;
                        }
                   }
                }
            }


            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                Time now = new Time();
                now.setToNow();
                Log.e("com.pegright.mobile.o", "Starting Page Load for: " + url + " at " + now.toString());
            }
        });


        if (intent.getData() != null) {
            webview.loadUrl(intent.getDataString());

            //CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
            //CustomTabsIntent customTabsIntent = builder.build();
            //customTabsIntent.launchUrl(this, intent.getData());



        }


    }
}